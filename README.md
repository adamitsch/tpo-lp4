# Lastni projekt pri predmetu TPO

Vsaka skupina, ki je sestavljena iz 4 članov, mora razviti lastni projekt (LP) na izbrani problemski domeni, in sicer od **predloga projekta** do **implementacije**, kjer je podrobna razdelitev naslednja:

* **1. LP** - [Predlog projekta](docs/predlog-projekta),
* **2. LP** - [Zajem zahtev](docs/zajem-zahtev),
* **3. LP** - [Načrt rešitve](docs/nacrt) in
* **4. LP** - [Implementacija](src).
---

[povezava na aplikacijo](http://tpo-straightas.herokuapp.com/)

Vpisni podatki študenta:

    email: student@gmail.com
    geslo: abc
    
Vpisni podatki upravljalca dogodkov:

    email: manager@gmail.com
    geslo: abc
    
Vpisni podatki administratorja:

    email: admin@gmail.com
    geslo: abc
    
Na strani '/db' se lahko ponastavi bazo na začetni vnos.
