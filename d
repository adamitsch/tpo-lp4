[33mcommit 51c7c28c87e9f209e5728667cf32c881e6a24523[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m, [m[1;31morigin/master[m[33m, [m[1;31morigin/HEAD[m[33m)[m
Merge: 23a2b84 888d492
Author: Jan <ja9744@student.uni-lj.si>
Date:   Sun May 26 11:56:36 2019 +0200

    prikaz bliznjih restavracij #29

[33mcommit 23a2b8456929e036b1c0a63782c09b4ebbef8da3[m
Author: Jan <ja9744@student.uni-lj.si>
Date:   Sun May 26 11:55:07 2019 +0200

    ogled bližnjih restavracij #29

[33mcommit 888d492af614cf3dcc30e3112df9a98dd7851a36[m
Author: unknown <janlovsin@gmail.com>
Date:   Sun May 26 11:40:26 2019 +0200

    odstarnjen gumb #5

[33mcommit 7898a0ed16b34d9e4ff3d046150875018ca56ef0[m
Author: unknown <janlovsin@gmail.com>
Date:   Sun May 26 11:39:21 2019 +0200

    ugly avtobusi #22 #16 #5

[33mcommit 1731a72a996bb624ed600b29421091e3d3c0920d[m
Merge: 3dbc67b eab56e0
Author: Mike72 <mihaelrajh@gmail.com>
Date:   Sat May 25 23:46:38 2019 +0200

    Merge branch 'master' of bitbucket.org:adamitsch/tpo-lp4

[33mcommit 3dbc67b8f4440a75ec917f2303a94ba179a5b58a[m
Author: Mike72 <mihaelrajh@gmail.com>
Date:   Sat May 25 23:46:30 2019 +0200

    Dodal odstranjevanje dogodkov s koledarja (#6). Manjka še dodajanje in urejanje, ter prevod v slo heh.

[33mcommit eab56e0153125a97102c860137a602d3e9c3198f[m
Author: Jan <ja9744@student.uni-lj.si>
Date:   Sat May 25 22:49:19 2019 +0200

    gumb

[33mcommit 351a8b04e064b40bf361e96c3b1a01413e494bc8[m
Author: Jan <ja9744@student.uni-lj.si>
Date:   Sat May 25 22:38:45 2019 +0200

    sprememba gesla fertig #28

[33mcommit bf1bfd7963c01880b3898703b21e464d5ed9153c[m
Author: Jan <ja9744@student.uni-lj.si>
Date:   Sat May 25 21:46:16 2019 +0200

    sprememba gesla skoraj #28

[33mcommit 0b1311d7ba761491089a77da9dc020af91626b0f[m
Merge: f291988 8bac8ef
Author: Mike72 <mihaelrajh@gmail.com>
Date:   Sat May 25 20:36:32 2019 +0200

    Merge branch 'master' of bitbucket.org:adamitsch/tpo-lp4

[33mcommit f291988253d47ca013c147e02f9bcd8451646a0b[m
Author: Mike72 <mihaelrajh@gmail.com>
Date:   Sat May 25 20:36:16 2019 +0200

    Added interface for viewing scheduled events on calendar. Missing adding, editing and removing events.

[33mcommit 8bac8ef2999d5302b6636a3aca5bfaea6211c608[m
Merge: e04324e 5c8ca47
Author: unknown <janlovsin@gmail.com>
Date:   Sat May 25 19:50:25 2019 +0200

    neki za bus

[33mcommit e04324ee403687592d244120b11ba6c25802a944[m
Author: unknown <janlovsin@gmail.com>
Date:   Sat May 25 19:38:13 2019 +0200

    neparsan bus loc

[33mcommit 5c8ca47ce0beda15993a6bb1b130768ba5c8e0d3[m
Merge: 0ad1d4a d916db2
Author: Jan <ja9744@student.uni-lj.si>
Date:   Sat May 25 19:36:18 2019 +0200

    Merge branch 'master' of https://bitbucket.org/adamitsch/tpo-lp4

[33mcommit 0ad1d4a15296c34044cfec1768a7fc8c40d1f067[m
Author: Jan <ja9744@student.uni-lj.si>
Date:   Sat May 25 19:36:08 2019 +0200

    prijava #27

[33mcommit d916db259a446bbe45b7b3b342ca452d338633fc[m
Merge: 8cd3b9c dee2d2b
Author: Mike72 <mihaelrajh@gmail.com>
Date:   Sat May 25 17:43:04 2019 +0200

    Merge branch 'master' of bitbucket.org:adamitsch/tpo-lp4

[33mcommit 8cd3b9cae27ab9295941a7592dada35566d54310[m
Author: Mike72 <mihaelrajh@gmail.com>
Date:   Sat May 25 17:42:39 2019 +0200

    Added some API for calendar #14 #25.

[33mcommit dee2d2bcb5fb152d1848e2f7b47e075620f6fb00[m
Author: Jan <ja9744@student.uni-lj.si>
Date:   Sat May 25 16:22:42 2019 +0200

    zamenjan api key #5

[33mcommit e07e0e31bb1b8cd0314b597c8defdde06a281b57[m
Merge: 4057f5b 31d8292
Author: Mike72 <mihaelrajh@gmail.com>
Date:   Sat May 25 16:10:37 2019 +0200

    Merged.

[33mcommit 4057f5b5608fec3c25b2695411dd8ce7f5050153[m
Author: Mike72 <mihaelrajh@gmail.com>
Date:   Sat May 25 16:04:11 2019 +0200

    Implemented interactive calendar rendering. Missing calendar events and functionality. #6

[33mcommit 31d82923feb2073c1d3f7be09003dd3555708db7[m
Author: unknown <janlovsin@gmail.com>
Date:   Sat May 25 15:45:52 2019 +0200

    maps #5, api key needed

[33mcommit 8e2e926db6034e54f3a07cb026d36b302bccea72[m
Author: Mike72 <mihaelrajh@gmail.com>
Date:   Sat May 25 13:37:13 2019 +0200

    Adjusted home, registration and login layouts. Began working on home view #6.

[33mcommit bfed3befd4adf9fc338c8a484aa4eab810d2deb1[m
Author: unknown <janlovsin@gmail.com>
Date:   Fri May 24 21:16:13 2019 +0200

    upravljanje z dogodki #8 #31 #12

[33mcommit 7ef925881a85d454cfe75ef126df20221de0aff2[m
Author: Jan <ja9744@student.uni-lj.si>
Date:   Thu May 23 14:08:17 2019 +0200

    gumb

[33mcommit e667fd5d269a9bdc1ec5955b22a990acb3279b2f[m
Author: Jan <ja9744@student.uni-lj.si>
Date:   Thu May 23 13:35:42 2019 +0200

    api

[33mcommit 2765121dc2f8a9752e84dd604aaceabccc4daa70[m
Author: Jan Adamič <ja9744@student.uni-lj.si>
Date:   Thu May 16 12:30:15 2019 +0200

    povezava na aplikacijo

[33mcommit 427346cbc9a3400a9f3c78e87824940fc581e95a[m
Merge: f36fac1 188abf4
Author: Jan Adamič <ja9744@student.uni-lj.si>
Date:   Thu May 16 11:43:00 2019 +0200

    karnek

[33mcommit f36fac16a7eba38f2ec9c4259b276a929ef84163[m
Author: Jan Adamič <ja9744@student.uni-lj.si>
Date:   Thu May 16 11:41:29 2019 +0200

    abc

[33mcommit 56f9c7f8da70a5f480ef05bebe38a9829940b457[m
Author: Jan Adamič <ja9744@student.uni-lj.si>
Date:   Thu May 16 11:30:20 2019 +0200

    test testa

[33mcommit 188abf4d15d09616543ea28148ef3b3e7e039983[m
Author: unknown <janlovsin@gmail.com>
Date:   Thu May 16 11:20:56 2019 +0200

    Vse strani sam preveri status

[33mcommit fec69c453f1f5a9ffd819b0e3a2dcb612c9a69ef[m
Merge: 905b51f 8255ff1
Author: unknown <janlovsin@gmail.com>
Date:   Thu May 16 11:12:23 2019 +0200

    Merge branch 'master' of https://bitbucket.org/adamitsch/tpo-lp4

[33mcommit 905b51fae56edd1313eaa40f5bb209e06f4f01f4[m
Author: unknown <janlovsin@gmail.com>
Date:   Thu May 16 11:12:05 2019 +0200

    Testi za pristnost strani

[33mcommit 8255ff1e52efc2a986d187ac093cba34708c1ff0[m
Merge: 74a4c3e 7666ee3
Author: Jan Adamič <ja9744@student.uni-lj.si>
Date:   Thu May 16 10:57:39 2019 +0200

    Merge branch 'master' of https://bitbucket.org/adamitsch/tpo-lp4

[33mcommit 74a4c3ec4495f9359681e505735ec177198f9487[m
Author: Jan Adamič <ja9744@student.uni-lj.si>
Date:   Thu May 16 10:57:28 2019 +0200

    modeli

[33mcommit 7666ee34551c1da5f031d4185c157bde9500776f[m
Author: unknown <janlovsin@gmail.com>
Date:   Thu May 16 10:12:47 2019 +0200

    Brisanje dogodkov

[33mcommit 60705c6d67ecb657a0eceb4fa4d5e986bb066021[m
Merge: f14fe08 2e95845
Author: unknown <janlovsin@gmail.com>
Date:   Thu May 16 09:45:23 2019 +0200

    t merge --abort
    git merge --abort
    :Q
    
    Merge branch 'master' of https://bitbucket.org/adamitsch/tpo-lp2

[33mcommit f14fe08d8693eb8525d11fac59fdf6f6dc3d7166[m
Author: unknown <janlovsin@gmail.com>
Date:   Thu May 16 09:45:02 2019 +0200

    Moznost dodajanje dogodkov in prikat

[33mcommit 2e95845c4d12b6c109a2edf2f222f31d612fa70c[m
Author: Jan Adamič <ja9744@student.uni-lj.si>
Date:   Thu May 16 09:26:40 2019 +0200

    tab

[33mcommit da7c9386dc92f669e1cfd6e5caf39e0f6336a82d[m
Author: Jan Adamič <ja9744@student.uni-lj.si>
Date:   Thu May 16 09:12:03 2019 +0200

    cicd test

[33mcommit a01a451148807353072b12a187f0b6ad0dd99c5f[m
Author: Jan Adamič <ja9744@student.uni-lj.si>
Date:   Thu May 16 08:37:38 2019 +0200

    dinamicen port

[33mcommit 7c981bf99f970109e18beba133b902dffd55daed[m
Author: Jan Adamič <ja9744@student.uni-lj.si>
Date:   Thu May 16 08:25:11 2019 +0200

    a to dela

[33mcommit 84d10bfceb17b69dc5564b0cd3d7c986b0f02b4e[m
Author: Jan Adamič <ja9744@student.uni-lj.si>
Date:   Thu May 16 08:01:36 2019 +0200

    heroku

[33mcommit dcfbbd78940695c73d1e58ea2bfc9846929ef31e[m
Author: Jan Adamič <ja9744@student.uni-lj.si>
Date:   Thu May 16 07:30:18 2019 +0200

    test

[33mcommit 898cdbf53bb57258004b916b62a66b675254ca99[m
Author: unknown <janlovsin@gmail.com>
Date:   Thu May 16 01:27:14 2019 +0200

    manjsi glitch

[33mcommit 5d794f3119240b4b06f875b432a844ac86dd38d9[m
Merge: 050bcb1 6843165
Author: unknown <janlovsin@gmail.com>
Date:   Thu May 16 01:21:30 2019 +0200

    resolved conflicts

[33mcommit 050bcb1ad697d9f67b588d8324163abd6a663256[m
Author: unknown <janlovsin@gmail.com>
Date:   Thu May 16 01:12:23 2019 +0200

    Povezane strani

[33mcommit 68431655ceb79610659ec909ed3bbbf1b9ba1fb5[m
Author: Jan <ja9744@student.uni-lj.si>
Date:   Wed May 15 23:58:25 2019 +0200

    dodajanje uporabnika

[33mcommit da630f9d80a0023d7282fa1ea1ed1961b74423ed[m
Merge: a8cae6b 6062644
Author: Jan <ja9744@student.uni-lj.si>
Date:   Wed May 15 23:48:13 2019 +0200

    Merge branch 'master' of https://bitbucket.org/adamitsch/tpo-lp4

[33mcommit a8cae6b3936295923484656eba2517b3ac4595a6[m
Author: Jan <ja9744@student.uni-lj.si>
Date:   Wed May 15 23:48:02 2019 +0200

    skor registracija

[33mcommit 6062644b90be3d27a9f5eb4c22437a74012762e1[m
Author: unknown <janlovsin@gmail.com>
Date:   Wed May 15 23:45:35 2019 +0200

    Api controller

[33mcommit f3827ae08a16b714d26d92607cde649ea4b3aed8[m
Author: Jan <ja9744@student.uni-lj.si>
Date:   Wed May 15 22:46:40 2019 +0200

    api

[33mcommit e4de3eab40440426f2ebff538e1413c539fcbbe3[m
Author: Jan <ja9744@student.uni-lj.si>
Date:   Wed May 15 22:05:18 2019 +0200

    nc kaj tazga

[33mcommit 54d76cd491350d7650ac3599162e168d8c325e91[m
Author: Jan <ja9744@student.uni-lj.si>
Date:   Wed May 15 21:53:03 2019 +0200

    a mormo prou pisat kaj delamo v commitih

[33mcommit ed8b6f6cc869b5fbfca7acc74e4d261b4397e574[m
Author: Jan <ja9744@student.uni-lj.si>
Date:   Wed May 15 21:36:28 2019 +0200

    pug

[33mcommit 0b0a31208835edd7ee0b9285fefa927e83534676[m
Author: Dejan Sinic <sinic.dejan@gmail.com>
Date:   Wed May 15 15:05:32 2019 +0000

    Wireframe za prijavo, osnovna funkcionalnost ref #2

[33mcommit 2986925f59de83e89558b670adb8ce42ed134912[m
Author: Dejan Sinic <sinic.dejan@gmail.com>
Date:   Wed May 15 15:00:59 2019 +0000

    Wireframe za registracijo, osnovna funkcionalnost ref #1

[33mcommit ef9d555b9c0d00ade8346d41268021f58a2bbb16[m
Author: Dejan Sinic <sinic.dejan@gmail.com>
Date:   Wed May 15 14:51:17 2019 +0000

    Dodan app client

[33mcommit a7969e91a18f2ad404a15ba479248ed4dfa61b3d[m
Author: Jan Adamič <ja9744@student.uni-lj.si>
Date:   Wed May 15 16:16:31 2019 +0200

    model

[33mcommit 6800c802ce5795e532b9c0c04cb29948e1e503c2[m
Merge: c542022 b38f7cc
Author: Jan Adamič <ja9744@student.uni-lj.si>
Date:   Wed May 15 15:25:54 2019 +0200

    apppiii

[33mcommit c542022d01f6b845d41d7e52ed2ccf8df84b35f8[m
Author: Jan Adamič <ja9744@student.uni-lj.si>
Date:   Wed May 15 15:22:19 2019 +0200

    api

[33mcommit b38f7cc38a7a7c49913967ff774a0a12921ce96c[m
Author: unknown <janlovsin@gmail.com>
Date:   Wed May 15 14:58:31 2019 +0200

    Dodan delni api

[33mcommit 56d9660e16b743e4592a23f19cc2e3b424a2d91d[m[33m ([m[1;31morigin/production[m[33m, [m[1;32mproduction[m[33m)[m
Author: unknown <janlovsin@gmail.com>
Date:   Wed May 15 14:01:17 2019 +0200

    Premaknjeni testi

[33mcommit 257715a3048aa26071f474f5dc6ae1b24d4225e5[m
Author: unknown <janlovsin@gmail.com>
Date:   Wed May 15 13:54:35 2019 +0200

    Testi popravljeni

[33mcommit 69feb87e3b064503162ad0174f88858be54c4dff[m
Author: unknown <janlovsin@gmail.com>
Date:   Wed May 15 13:01:04 2019 +0200

    Rearanged datoteke

[33mcommit 82e985ad5acb2e6724bfe79d195dd2c9cd9a3a1e[m
Author: unknown <janlovsin@gmail.com>
Date:   Wed May 15 12:29:43 2019 +0200

    Inicializacija lp4

[33mcommit db72acedd369074820c6c125eeea22844d62d87b[m
Author: Jan Adamic <ja9744@student.uni-lj.si>
Date:   Wed May 8 18:37:51 2019 +0000

    bitbucket-pipelines.yml edited online with Bitbucket

[33mcommit e4934653cdb61edd1042d62354c292f2c647bbe7[m
Author: Jan Adamic <ja9744@student.uni-lj.si>
Date:   Wed May 8 18:31:39 2019 +0000

    Initial Bitbucket Pipelines configuration

[33mcommit 71c68d6a82c6b8c85e9ad4b62c901e13bdec0273[m
Author: Jan Adamič <ja9744@student.uni-lj.si>
Date:   Wed May 8 20:26:49 2019 +0200

    test

[33mcommit e532000314c98f35300895705b32ac8afa1fba44[m
Author: Jan Adamič <ja9744@student.uni-lj.si>
Date:   Wed May 8 20:11:48 2019 +0200

    init

[33mcommit ec838da3af246d5044e6cf7bd4b70e35214ad6cf[m
Author: Dejan Lavbič <Dejan@lavbic.net>
Date:   Thu Apr 25 06:30:09 2019 +0200

    Začetna verzija z izbrano najboljšo specifikacijo projekta iz 2. LP in 3. LP
