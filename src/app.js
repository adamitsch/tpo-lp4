var express = require('express');
var path = require('path');

require('./app_api/models/db');

var indexRouter = require('./app_server/routes/index');
var apiRouter = require('./app_api/routes/index');

var createError = require('http-errors');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const session = require('express-session');


const PORT = process.env.PORT || 7000;


//baza



// Init app
var app = express();


// Load view engine
app.set('views', path.join(__dirname, 'app_server', 'views'))
app.set('view engine', 'pug');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
  name: "straightas",
  resave: false,
  saveUninitialized: false,
  secret: "skrivnogeslo",
  cookie : {
    //ena ura
    maxAge: 1000*60*60,
    sameSite: true
  }
}))


app.use('/', indexRouter);
app.use('/api', apiRouter);

var server = app.listen(PORT, () => {
  console.log(`Express running → PORT ${server.address().port}`);
});

var adre = server.address();

module.exports = app;