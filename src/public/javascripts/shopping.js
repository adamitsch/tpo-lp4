var storageIds = JSON.parse(localStorage.getItem("selectedProductsId"));
var storageAmounts = JSON.parse(localStorage.getItem("selectedProductsAmount"));
var storagePrice = [];

var currency = "€";
function setCurrency(product_currency) {
    currency = product_currency;
}

var totalPrice = 0;
function totalPriceAdd(product_id, product_price) {
    var index = storageIds.indexOf(product_id);
    if (index > -1) {
        totalPrice += (product_price * storageAmounts[index]);
        document.getElementById(product_id+"a").value = storageAmounts[index];
        storagePrice[index] = product_price;
    }
}

function up(product_id) {
    document.getElementById(product_id+"a").value = Number(document.getElementById(product_id+"a").value) + 1;
    setAmountValue(product_id);
}

function down(product_id) {
    document.getElementById(product_id+"a").value = Number(document.getElementById(product_id+"a").value) - 1;
    setAmountValue(product_id);
}

function setTotalPrice() {
    document.getElementById("totalPrice").textContent = " " + totalPrice.toFixed(2) + "  "  + currency;
}

function setSingleItemPrice(product_id) {
    var index = storageIds.indexOf(product_id);
    var singlePrice = 0;
    if (index > -1) {
        singlePrice = storagePrice[index] * storageAmounts[index];
    }

    document.getElementById(product_id+"pp").textContent = singlePrice.toFixed(2) + "  " + currency;
}

function emptyBasket() {
    var emptyArray = [];
    localStorage.setItem("selectedProductsId", JSON.stringify(emptyArray));
    localStorage.setItem("selectedProductsAmount", JSON.stringify(emptyArray));
}

function setAmountValue(product_id) {
    var index = storageIds.indexOf(product_id);
    var value = Number(document.getElementById(product_id+"a").value);
    if (value < 1) value = 1;
    if (value > 99) value = 99;
    document.getElementById(product_id+"a").value = value;
    storageAmounts[index] = value;
    localStorage.setItem("selectedProductsAmount", JSON.stringify(storageAmounts));

    setSingleItemPrice(product_id);
    newTotalPrice();
}

function newTotalPrice() {
    totalPrice = 0;
    for (var i = 0; i < storageIds.length; i++) {
        if (storagePrice[i] != null) {
            totalPrice += (storagePrice[i] * storageAmounts[i]);
        }
    }
    setTotalPrice();
}

function getProductParams(params) {
    var ret = JSON.parse(localStorage.getItem(params));
    if (ret == null)
        return [];
    return ret;
}

function removeProduct(product_id) {
    var index = storageIds.indexOf(product_id);
    storageIds.splice(index, 1);
    storageAmounts.splice(index, 1);
    localStorage.setItem("selectedProductsId", JSON.stringify(storageIds));
    localStorage.setItem("selectedProductsAmount", JSON.stringify(storageAmounts));
    storagePrice.splice(index, 1);

    newTotalPrice();
    removeProductElement(product_id);
}

function removeProductElement(product_id) {
    var productIds = getProductParams("selectedProductsId");
    if (!productIds.includes(product_id)) {
        var child = document.getElementById(product_id);
        if (child != null) {
            child.parentNode.removeChild(child);
        }
    }
}

function addProduct(product_id) {
    var storedProductsId = JSON.parse(localStorage.getItem("selectedProductsId"));
    var storedProductsAmount = JSON.parse(localStorage.getItem("selectedProductsAmount"));
    if (storedProductsId == null) {
        storedProductsId = [];
        storedProductsAmount = [];
        storedProductData = [];
    }
    var exists = storedProductsId.indexOf(product_id);
    if (exists > -1) {
        // storedProductsAmount[exists] += 1;
    } else {
        storedProductsId.push(product_id);
        storedProductsAmount.push(1);
    }

    localStorage.setItem("selectedProductsId", JSON.stringify(storedProductsId));
    localStorage.setItem("selectedProductsAmount", JSON.stringify(storedProductsAmount));

    changeButton(product_id);
}

function changeButton(product_id) {
    var storedProductsId = JSON.parse(localStorage.getItem("selectedProductsId"));

    if (storedProductsId.indexOf(product_id) > -1) {
        document.getElementById('13').classList.remove("btn-warning");
        document.getElementById('13').classList.add("btn-success");
        document.getElementById("13").textContent = "V košarici";
    } else {
        document.getElementById('13').classList.remove("btn-success");
        document.getElementById('13').classList.add("btn-warning");
        document.getElementById("13").textContent = "Dodaj v košarico";
    }

}

function submitOrder() {
    document.getElementById('order').hidden = false;
    if (getUserId() != null) {
        if (storageIds.length == 0) {
            document.getElementById('order').classList.remove("alert-success");
            document.getElementById('order').classList.add("alert-warning");
            document.getElementById('order').textContent = "Prazna košarica!";
        } else {
            document.getElementById('order').classList.remove("alert-warning");
            document.getElementById('order').classList.add("alert-success");
            document.getElementById('order').textContent = "Naročilo poslano v obdelavo!";

            for (var i = storageIds.length - 1; i >= 0; i--) {
                removeProduct(storageIds[i]);
            }
        }
    } else {
        document.getElementById('order').classList.remove("alert-success");
        document.getElementById('order').classList.add("alert-warning");
        document.getElementById("order").textContent = "Potrebna prijava!";
    }
}