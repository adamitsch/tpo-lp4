var months = [];
months[0] = "januar";
months[1] = "februar";
months[2] = "marec";
months[3] = "april";
months[4] = "maj";
months[5] = "junij";
months[6] = "julij";
months[7] = "avgust";
months[8] = "september";
months[9] = "oktober";
months[10] = "november";
months[11] = "december";

var currentMonth;
var currentYear;
var url;
var userId;
var calendarEvents = [];

var createEvent = function(day) {
    $("#create-calendar-title").text("Nov vnos");
    $("#calendar-create-ime").val("");
    $("#calendar-create-cas").val("12:30");
    $("#calendar-create-trajanje").val("00:20");
    $("#calendar-create-opis").val("");
    $("#create-warning").text("");
    document.querySelectorAll("#confirm-create-calendar")
        .forEach(function(btn) {
            $(btn).off();
            $(btn).on("click", function() {
                var ime = $("#calendar-create-ime").val();
                var cas = $("#calendar-create-cas").val();
                var trajanje = $("#calendar-create-trajanje").val();
                var opis = $("#calendar-create-opis").val();

                var warning = "";
                if(!(ime.trim()))
                    warning += "Prosim vnesite ime. ";

                var uraCas = parseInt(cas.substring(0,2));
                var minutaCas = parseInt(cas.substring(3,5));
                if(cas.length !== 5 || isNaN(uraCas) || isNaN(minutaCas) || uraCas < 0 || uraCas > 23
                    || minutaCas < 0 || minutaCas > 59)
                    warning += "Neveljaven čas. Mora biti oblike '12:34'. ";

                var uraTrajanja = parseInt(trajanje.substring(0,2));
                var minutaTrajanja = parseInt(trajanje.substring(3,5));
                if(trajanje.length !== 5 || isNaN(uraTrajanja) || isNaN(minutaTrajanja)
                    || uraTrajanja < 0 || uraTrajanja > 23 || minutaTrajanja < 0 || minutaTrajanja > 59)
                    warning += "Neveljavno trajanje. Mora biti oblike '12:34'.";

                if(warning.length > 0)
                    $("#create-warning").text(warning);
                else {
                    if(!(opis.trim()))
                        opis = "/";
                    $.ajax({
                        type: 'POST',
                        url: url + "/api/koledar/",
                        dataType: 'json',
                        contentType: 'application/json',
                        data: JSON.stringify({
                            "userId": userId,
                            "ime": ime,
                            "leto": currentYear,
                            "mesec": currentMonth,
                            "dan": day,
                            "cas": cas,
                            "trajanje": trajanje,
                            "opis": opis
                            }),
                        success: function( data, textStatus, jQxhr ){
                            var monthYear = ("0" + currentMonth).slice(-2) + "-" + ("000" + currentYear).slice(-4);
                            $.get(url + "/api/koledar/" + userId + "/" + monthYear, function(data) {
                                calendarEvents = data;
                                renderCalendar();
                            });
                            $("#info-modal-body").text("Vnos ustvarjen.");
                            document.querySelectorAll("#exit-info-modal")
                                .forEach(function(btn) {
                                    $(btn).off();
                                    $(btn).on("click", function() {
                                        $("#info-modal").modal("hide");
                                        showEvents(day);
                                    });
                                });
                            $("#calendar-create-modal").modal("hide");
                            $("#info-modal").modal();
                        },
                        error: function( jqXhr, textStatus, errorMessage ){
                            $("#info-modal-body").text('Napaka: ' + errorMessage);
                            document.querySelectorAll("#exit-info-modal")
                                .forEach(function(btn) {
                                    $(btn).off();
                                    $(btn).on("click", function() {
                                        $("#info-modal").modal("hide");
                                        showEvents(day);
                                    });
                                });
                            $("#calendar-create-modal").modal("hide");
                            $("#info-modal").modal();
                        }
                    });
                }
            });
        });

    $("#calendar-modal .close").click();
    $("#calendar-create-modal").modal();
};

var editEvent = function(event) {
    $("#create-calendar-title").text("Uredi vnos");
    $("#calendar-create-ime").val(event.ime);
    $("#calendar-create-cas").val(event.cas.replace("-", ":"));
    $("#calendar-create-trajanje").val(event.trajanje.replace("-", ":"));
    $("#calendar-create-opis").val(event.opis);
    $("#create-warning").text("");

    document.querySelectorAll("#confirm-create-calendar")
        .forEach(function(btn) {
            $(btn).off();
            $(btn).on("click", function() {
                var ime = $("#calendar-create-ime").val();
                var cas = $("#calendar-create-cas").val();
                var trajanje = $("#calendar-create-trajanje").val();
                var opis = $("#calendar-create-opis").val();

                var warning = "";
                if(!(ime.trim()))
                    warning += "Prosim vnesite ime. ";
                var uraCas = parseInt(cas.substring(0,2));
                var minutaCas = parseInt(cas.substring(3,5));
                if(cas.length !== 5 || isNaN(uraCas) || isNaN(minutaCas) || uraCas < 0 || uraCas > 23
                    || minutaCas < 0 || minutaCas > 59)
                    warning += "Neveljaven čas. Mora biti oblike '12:34'. ";

                var uraTrajanja = parseInt(trajanje.substring(0,2));
                var minutaTrajanja = parseInt(trajanje.substring(3,5));
                if(trajanje.length !== 5 || isNaN(uraTrajanja) || isNaN(minutaTrajanja)
                    || uraTrajanja < 0 || uraTrajanja > 23 || minutaTrajanja < 0 || minutaTrajanja > 59)
                    warning += "Neveljavno trajanje. Mora biti oblike '12:34'.";

                if(warning.length > 0)
                    $("#create-warning").text(warning);
                else {
                    if(!(opis.trim()))
                        opis = "/";
                    $.ajax({
                        type: 'PUT',
                        url: url + "/api/koledar/" + event.id,
                        dataType: 'json',
                        contentType: 'application/json',
                        data: JSON.stringify({
                            "ime": ime,
                            "cas": cas,
                            "trajanje": trajanje,
                            "opis": opis
                        }),
                        success: function( data, textStatus, jQxhr ){
                            var monthYear = ("0" + currentMonth).slice(-2) + "-" + ("000" + currentYear).slice(-4);
                            $.get(url + "/api/koledar/" + userId + "/" + monthYear, function(data) {
                                calendarEvents = data;
                                renderCalendar();
                            });
                            $("#info-modal-body").text("Vnos posodobljen.");
                            document.querySelectorAll("#exit-info-modal")
                                .forEach(function(btn) {
                                    $(btn).off();
                                    $(btn).on("click", function() {
                                        $("#info-modal").modal("hide");
                                        showEvents(event.dan);
                                    });
                                });
                            $("#calendar-create-modal").modal("hide");
                            $("#info-modal").modal();
                        },
                        error: function( jqXhr, textStatus, errorMessage ){
                            $("#info-modal-body").text('Napaka: ' + errorMessage);
                            document.querySelectorAll("#exit-info-modal")
                                .forEach(function(btn) {
                                    $(btn).off();
                                    $(btn).on("click", function() {
                                        $("#info-modal").modal("hide");
                                        showEvents(event.dan);
                                    });
                                });
                            $("#calendar-create-modal").modal("hide");
                            $("#info-modal").modal();
                        }
                    });
                }
            });
        });

    $("#calendar-modal .close").click();
    $("#calendar-create-modal").modal();
};

var removeEvent = function(event) {
    $("#confirm-delete-calendar").on("click", function() {
        $.ajax({
            type: 'DELETE',
            url: url + "/api/koledar/" + event.id,
            success: function(data, status){
                var monthYear = ("0" + currentMonth).slice(-2) + "-" + ("000" + currentYear).slice(-4);
                $.get(url + "/api/koledar/" + userId + "/" + monthYear, function(data) {
                    calendarEvents = data;
                    renderCalendar();
                });
                $("#info-modal-body").text("Vnos izbrisan.");
                document.querySelectorAll("#exit-info-modal")
                    .forEach(function(btn) {
                        $(btn).off();
                        $(btn).on("click", function() {
                            $("#info-modal").modal("hide");
                            showEvents(event.dan);
                        });
                    });
                $("#calendar-delete-modal").modal("hide");
                $("#info-modal").modal();
            },
            error: function (jqXhr, textStatus, errorMessage) {
                $("#info-modal-body").text('Napaka: ' + errorMessage);
                document.querySelectorAll("#exit-info-modal")
                    .forEach(function(btn) {
                        $(btn).off();
                        $(btn).on("click", function() {
                            $("#info-modal").modal("hide");
                            showEvents(event.dan);
                        });
                    });
                $("#calendar-delete-modal").modal("hide");
                $("#info-modal").modal();
            }
        });
    });
    $("#calendar-modal .close").click();
    $("#calendar-delete-modal").modal();

};

var formatDuration = function(t) {
  var hours = parseInt(t.substring(0, 2));
  var minutes = parseInt(t.substring(3, 5));
  if(minutes === 0 && hours === 0)
      return "0min";
  else if(minutes === 0)
      return String(hours) + "h";
  else {
      var formatted = String(minutes) + "min";
      if(hours > 0)
          formatted = String(hours) + "h " + formatted;
  }
  return formatted;
};

var formatTime = function (t) {
    var hours = parseInt(t.substring(0, 2));
    var minutes = parseInt(t.substring(3, 5));
    return String(hours) + ":" + minutes;
};

var showEvents = function(day) {
    $("#calendar-modal-title").text(String(currentYear) + "/" + ("0" + (currentMonth + 1)).slice(-2) + "/" +
        ("0" + day).slice(-2));

    var dayEvents = new Array();
    calendarEvents.forEach(function(event) {
        if(event.dan === parseInt(day))
            dayEvents.push(event);
    });

    $(document.querySelector("#create-calendar")).off()
        .on("click", function() {
            createEvent(day);
        });

    var modalBody = document.querySelector(".modal-body-content");
    modalBody.innerHTML = "";
    if(dayEvents.length === 0) {
        var p = document.createElement("p");
        p.innerHTML = "Na ta dan ni nobenih dogodkov.";
        modalBody.appendChild(p);
    } else {
        dayEvents.forEach(function(event) {
           var div = document.createElement("div");

           div.classList.add("calendar-event");
           div.setAttribute("id", event.id);
           var h4 = document.createElement("h4");
           h4.innerText = event.ime;
           var time = document.createElement("p");
           time.innerText = "Ob " + formatTime(event.cas) + ", traja " + formatDuration(event.trajanje) + ".";
           var opis = document.createElement("p");
           opis.innerText = "Opis: " + event.opis;

           var editBtn = document.createElement("button");
           editBtn.addEventListener("click", function(x) {
               editEvent(event);
           });
           editBtn.innerText = "Uredi";
           $(editBtn).addClass("btn").addClass("btn-info").addClass("margin-right-halfem");
           var removeBtn = document.createElement("button");
           removeBtn.addEventListener("click", function(x) {
               removeEvent(event);
           });
           removeBtn.innerText = "Odstrani";
           $(removeBtn).addClass("btn").addClass("btn-danger");

           div.appendChild(h4);
           div.appendChild(time);
           div.appendChild(opis);
           div.appendChild(editBtn);
           div.appendChild(removeBtn);

           modalBody.appendChild(div);
        });
    }

    $("#calendar-modal").modal();

};

var renderCalendar = function() {
    var year = String(currentYear);
    var month = months[currentMonth];
    document.querySelector(".selected-month").innerHTML = month + " " + year;

    var firstDay = (new Date(currentYear, currentMonth, 1)).getDay();
    if(firstDay === 0) // Sunday is 0
        firstDay = 7;
    firstDay--; // Monday is now 0

    var datesInCalendar = document.querySelector(".day");
    datesInCalendar.innerHTML = "";

    for(var i = 0; i < firstDay; i++) {
        var emptyLi = document.createElement("li");
        datesInCalendar.appendChild(emptyLi);
    }
    var daysInMonth = (new Date(currentYear, currentMonth + 1, 0)).getDate();

    var daysWithEvents = new Array();
    calendarEvents.forEach(function(event) {
        if(!daysWithEvents.includes(event.dan))
            daysWithEvents.push(event.dan);
    });

    for(var i = 1; i <= daysInMonth; i++) {
        var li = document.createElement("li");
        li.addEventListener("click", function(event) {
            showEvents(this.innerHTML);
        });
        if(daysWithEvents.includes(i))
            $(li).addClass("active-day");
        li.innerHTML = i;
        datesInCalendar.appendChild(li);
    }

    var totalNodes = 28;
    if(firstDay + daysInMonth > 35)
        totalNodes = 42;
    else if(firstDay + daysInMonth > 28)
        totalNodes = 35;
    for(var i = 0; i < totalNodes - daysInMonth - firstDay; i++) {
        var emptyLi = document.createElement("li");
        datesInCalendar.appendChild(emptyLi);
    }
};

var renderNextMonth = function() {
    currentMonth++;
    if(currentMonth === 12) {
        currentMonth = 0;
        currentYear++;
    }
    var monthYear = ("0" + currentMonth).slice(-2) + "-" + ("000" + currentYear).slice(-4);
    $.get(url + "/api/koledar/" + userId + "/" + monthYear, function(data) {
        calendarEvents = data;
        renderCalendar();
    });
};

var renderPrevMonth = function() {
    currentMonth--;
    if(currentMonth === -1) {
        currentMonth = 11;
        currentYear--;
    }
    var monthYear = ("0" + currentMonth).slice(-2) + "-" + ("000" + currentYear).slice(-4);
    $.get(url + "/api/koledar/" + userId + "/" + monthYear, function(data) {
        calendarEvents = data;
        renderCalendar();
    });
};

$(document).ready(function(){
    var today = new Date();
    currentMonth = today.getMonth();
    currentYear = today.getFullYear();
    url = window.location.origin;
    userId = $("input[name=userId]").val();

    var monthYear = ("0" + currentMonth).slice(-2) + "-" + ("000" + currentYear).slice(-4);
    $.get(url + "/api/koledar/" + userId + "/" + monthYear, function(data) {
        calendarEvents = data;
        renderCalendar();
    });

    document.querySelector(".next-month")
        .addEventListener("click", function(event) {
            renderNextMonth();
    });

    document.querySelector(".prev-month")
        .addEventListener("click", function(event) {
            renderPrevMonth();
        });

    document.querySelectorAll(".cancel-delete-calendar")
        .forEach(function(btn) {
            $(btn).on("click", function() {
                $("#calendar-delete-modal").modal("toggle");
                $("#calendar-modal").modal();
            });
        });

    document.querySelectorAll(".cancel-create-calendar")
        .forEach(function(btn) {
            $(btn).on("click", function() {
                $("#calendar-create-modal").modal("toggle");
                $("#calendar-modal").modal();
            });
        });
});
