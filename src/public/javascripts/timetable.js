var days = [];
days[0] = "pon";
days[1] = "tor";
days[2] = "sre";
days[3] = "čet";
days[4] = "pet";

var userId;
var timetableEvents = [];
var url;

var createTimetableEntry = function(day, time) {
    $("#create-timetable-title").text("Nov vnos");
    $("#timetable-create-ime").val("");
    $("#create-timetable-warning").text("");

    $('#timetable-create-dan option:selected').prop('selected', false);
    $('#timetable-create-cas option:selected').prop('selected', false);
    $('#timetable-create-trajanje option:selected').prop('selected', false);
    $('#timetable-create-barva option:selected').prop('selected', false);

    $("#timetable-create-dan option[value=" + day +"]").prop('selected', true);
    $("#timetable-create-cas option[value=" + time +"]").prop('selected', true);
    $("#timetable-create-trajanje option[value=1]").prop('selected', true);
    $("#timetable-create-barva option[value='/']").prop('selected', true);

    var deleteBtn = $("#confirm-delete-timetable");
    deleteBtn.off();
    deleteBtn.hide();
    var confirmBtn = $("#confirm-create-timetable");
    confirmBtn.off();
    confirmBtn.on("click", function() {
        var ime = $("#timetable-create-ime").val();
        var dan = $('#timetable-create-dan option:selected').val();
        var cas = $('#timetable-create-cas option:selected').val();
        var trajanje = $('#timetable-create-trajanje option:selected').val();
        var barva = $('#timetable-create-barva option:selected').val();

        var warning = "";
        if (!(ime.trim()))
            warning += "Prosim vnesite ime. ";

        if (parseInt(cas) + parseInt(trajanje) > 21)
            trajanje = 21 - parseInt(cas);

        var covered = false;
        for(var i = 0; i < trajanje; i++) {
            var id = "#" + days[dan] + String(parseInt(cas) + i);
            if($(id).text().trim().length > 0)
                covered = true;
        }
        console.log(covered);
        if(covered)
            warning += "Vnos se prekriva z že obstoječim vnosom v urniku!";

        if (warning.length > 0)
            $("#create-timetable-warning").text(warning);
        else {
            $.ajax({
                type: 'POST',
                url: url + "/api/urnik/",
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({
                    "userId": userId,
                    "ime": ime,
                    "dan": dan,
                    "cas": cas,
                    "trajanje": trajanje,
                    "barva": barva
                }),
                success: function (data, textStatus, jQxhr) {
                    $.get(url + "/api/urnik/" + userId, function (data) {
                        timetableEvents = data;
                        renderTimetable();
                    });
                    $("#info-modal-body").text("Vnos ustvarjen.");
                    document.querySelectorAll("#exit-info-modal")
                        .forEach(function (btn) {
                            $(btn).off();
                            $(btn).on("click", function () {
                                $("#info-modal").modal("hide");
                            });
                        });
                    $("#timetable-create-modal").modal("hide");
                    $("#info-modal").modal();
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    $("#info-modal-body").text('Napaka: ' + errorMessage);
                    document.querySelectorAll("#exit-info-modal")
                        .forEach(function (btn) {
                            $(btn).off();
                            $(btn).on("click", function () {
                                $("#info-modal").modal("hide");
                            });
                        });
                    $("#timetable-create-modal").modal("hide");
                    $("#info-modal").modal();
                }
            });
        }
    });
    $("#timetable-create-modal").modal();
};

var editTimetableEntry = function(event) {
    console.log(event.dan);
    console.log(event.cas);

    $("#create-timetable-title").text("Uredi vnos");
    $("#timetable-create-ime").val(event.ime);
    $("#create-timetable-warning").text("");

    $('#timetable-create-dan option:selected').prop('selected', false);
    $('#timetable-create-cas option:selected').prop('selected', false);
    $('#timetable-create-trajanje option:selected').prop('selected', false);
    $('#timetable-create-barva option:selected').prop('selected', false);

    $("#timetable-create-dan option[value=" + event.dan +"]").prop('selected', true);
    $("#timetable-create-cas option[value=" + event.cas +"]").prop('selected', true);
    $("#timetable-create-trajanje option[value=" + event.trajanje +"]").prop('selected', true);
    $("#timetable-create-barva option[value='" + event.barva + "']").prop('selected', true);

    var deleteBtn = $("#confirm-delete-timetable");
    deleteBtn.off();
    deleteBtn.on("click", function() {
        $.ajax({
            type: 'DELETE',
            url: url + "/api/urnik/" + event.id,
            success: function(data, status){
                $.get(url + "/api/urnik/" + userId, function (data) {
                    timetableEvents = data;
                    renderTimetable();
                });
                $("#info-modal-body").text("Vnos izbrisan.");
                document.querySelectorAll("#exit-info-modal")
                    .forEach(function(btn) {
                        $(btn).off();
                        $(btn).on("click", function() {
                            $("#info-modal").modal("hide");
                        });
                    });
                $("#timetable-create-modal").modal("hide");
                $("#info-modal").modal();
            },
            error: function (jqXhr, textStatus, errorMessage) {
                $("#info-modal-body").text('Napaka: ' + errorMessage);
                document.querySelectorAll("#exit-info-modal")
                    .forEach(function(btn) {
                        $(btn).off();
                        $(btn).on("click", function() {
                            $("#info-modal").modal("hide");
                        });
                    });
                $("#timetable-create-modal").modal("hide");
                $("#info-modal").modal();
            }
        });
    });
    deleteBtn.show();

    var confirmBtn = $("#confirm-create-timetable");
    confirmBtn.off();
    confirmBtn.on("click", function() {
        var ime = $("#timetable-create-ime").val();
        var dan = $('#timetable-create-dan option:selected').val();
        var cas = $('#timetable-create-cas option:selected').val();
        var trajanje = $('#timetable-create-trajanje option:selected').val();
        var barva = $('#timetable-create-barva option:selected').val();

        var warning = "";
        if (!(ime.trim()))
            warning += "Prosim vnesite ime. ";

        if (parseInt(cas) + parseInt(trajanje) > 21)
            trajanje = 21 - parseInt(cas);

        var originalCoverage = new Array();
        for(var i = 0; i < event.trajanje; i++) {
            var id = "#" + days[event.dan] + String(parseInt(event.cas) + i);
            originalCoverage.push(id);
        }

        var covered = false;
        for(var i = 0; i < trajanje; i++) {
            var id = "#" + days[dan] + String(parseInt(cas) + i);
            if(!originalCoverage.includes(id))
                if($(id).text().trim().length > 0)
                    covered = true;
        }
        console.log(covered);
        if(covered)
            warning += "Vnos se prekriva z že obstoječim vnosom v urniku!";

        if (warning.length > 0)
            $("#create-timetable-warning").text(warning);
        else {
            $.ajax({
                type: 'PUT',
                url: url + "/api/urnik/" + event.id,
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({
                    "ime": ime,
                    "dan": dan,
                    "cas": cas,
                    "trajanje": trajanje,
                    "barva": barva
                }),
                success: function (data, textStatus, jQxhr) {
                    $.get(url + "/api/urnik/" + userId, function (data) {
                        timetableEvents = data;
                        renderTimetable();
                    });
                    $("#info-modal-body").text("Vnos posodobljen.");
                    document.querySelectorAll("#exit-info-modal")
                        .forEach(function (btn) {
                            $(btn).off();
                            $(btn).on("click", function () {
                                $("#info-modal").modal("hide");
                            });
                        });
                    $("#timetable-create-modal").modal("hide");
                    $("#info-modal").modal();
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    $("#info-modal-body").text('Napaka: ' + errorMessage);
                    document.querySelectorAll("#exit-info-modal")
                        .forEach(function (btn) {
                            $(btn).off();
                            $(btn).on("click", function () {
                                $("#info-modal").modal("hide");
                            });
                        });
                    $("#timetable-create-modal").modal("hide");
                    $("#info-modal").modal();
                }
            });
        }
    });

    $("#timetable-create-modal").modal();
};

var renderTimetable = function() {
    var timetableBody = document.querySelector("#timetable-body");
    timetableBody.innerHTML = "";
    for(var i = 7; i < 21; i++) {
        var tr = document.createElement("tr");
        var trHead = document.createElement("td");
        $(trHead).text(String(i) + ":00");
        tr.appendChild(trHead);
        for(var j = 0; j < 5; j++) {
            var td = document.createElement("td");
            td.setAttribute("id", days[j] + i);
            $(td).addClass("hoverable");
            tr.appendChild(td);
            $(td).on("click", (function(i, j) {
                return function() {
                    createTimetableEntry(j, i);
                };
            })(i, j));
        }
        timetableBody.appendChild(tr);
    }

    timetableEvents.forEach(function(event) {
       var dan = event.dan;
       var cas = event.cas;
       var trajanje = event.trajanje;
       var barva = event.barva;

       for(var i = 0; i < trajanje; i++) {
           var id = "#" + days[dan] + String(cas+i);
           $(id).text(event.ime);
           $(id).off();
           $(id).on("click", function() {
              editTimetableEntry(event);
           });
           $(id).removeClass("table-warning", "table-danger", "table-primary", "table-success", "table-info");

           if(barva === "yel")
               $(id).addClass("table-warning");
           else if(barva === "red")
               $(id).addClass("table-danger");
           else if(barva === "blu")
               $(id).addClass("table-primary");
           else if(barva === "gre")
               $(id).addClass("table-success");
           else if(barva === "tur")
               $(id).addClass("table-info");
       }
    });
};

$(document).ready(function(){
    userId = $("input[name=userId]").val();
    url = window.location.origin;
    $.get(url + "/api/urnik/" + userId, function(data) {
        timetableEvents = data;
        renderTimetable();
    });
});
