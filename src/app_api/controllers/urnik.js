var mongoose = require('mongoose');

var Urnik = mongoose.model('Urnik');
var Uporabnik = mongoose.model('Uporabnik');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

module.exports.urnikUporabnik = function(zahteva, odgovor) {
    var idUporabnika = zahteva.params.idUporabnika;
    Urnik.find({
        userId: idUporabnika,
    }, function (napaka, rezultati) {
        seznam = [];
        if (napaka) {
            vrniJsonOdgovor(odgovor, 500, napaka);
        } else {
            rezultati.forEach(function (dokument) {
                seznam.push({
                    id: dokument._id,
                    ime: dokument.ime,
                    dan: dokument.dan,
                    cas: dokument.cas,
                    trajanje: dokument.trajanje,
                    barva: dokument.barva
                });
            });
            vrniJsonOdgovor(odgovor, 200, seznam);
        }
    });
};

module.exports.urnikKreiraj = function(zahteva, odgovor) {
    Uporabnik.findById(zahteva.body.userId)
        .exec(function(napaka, Uporabnik) {
            if (!Uporabnik) {
                vrniJsonOdgovor(odgovor, 400, {
                    "sporocilo": "Uporabnik s podanim identifikorjem ne obstaja."
                });
            } else if (napaka) {
                vrniJsonOdgovor(odgovor, 500, napaka);
            } else {
                var ime = zahteva.body.ime;
                var dan = parseInt(zahteva.body.dan);
                var cas = parseInt(zahteva.body.cas);
                var trajanje = parseInt(zahteva.body.trajanje);
                var barva = zahteva.body.barva;

                if(!(ime) || !(ime.trim()))
                    ime = "-";
                if(!(barva) || !(barva.trim()))
                    barva = "/";

                if(dan < 0 || dan > 4) {
                    vrniJsonOdgovor(odgovor, 400, {
                        "sporocilo": "Neveljaven dan. Mora biti v obsegu 0-4."
                    });
                } else if(cas < 7 || cas > 20) {
                    vrniJsonOdgovor(odgovor, 400, {
                        "sporocilo": "Neveljaven čas. Mora biti v obsegu 7-20."
                    });
                } else if(trajanje < 1 || trajanje+cas > 21) {
                    vrniJsonOdgovor(odgovor, 400, {
                        "sporocilo": "Neveljavno trajanje. Mora biti vsaj 1, in ne sme presegati 21-cas."
                    });
                } else Urnik.create ({
                    userId : zahteva.body.userId,
                    ime: ime,
                    dan: dan,
                    cas: cas,
                    trajanje: trajanje,
                    barva: barva
                }, function(napaka, Urnik) {
                    if (napaka) {
                        vrniJsonOdgovor(odgovor, 400, napaka);
                    } else {
                        vrniJsonOdgovor(odgovor, 201, Urnik);
                    }
                });
            }
        });
};

module.exports.urnikDelete = function (req, res) {
    var id = req.params.idUrnik;
    if (id) {
        Urnik.findByIdAndRemove(id)
            .exec(
                function(error, product) {
                    if (error) {
                        vrniJsonOdgovor(res, 404, error);
                        return;
                    }
                    vrniJsonOdgovor(res, 204, null);
                }
            );
    } else {
        vrniJsonOdgovor(res, 400, {
            "message":
                "Vnos ni bil najden."
        });
    }
};

module.exports.urnikPosodobi = function(zahteva, odgovor) {
    var ime = zahteva.body.ime;
    var dan = parseInt(zahteva.body.dan);
    var cas = parseInt(zahteva.body.cas);
    var trajanje = parseInt(zahteva.body.trajanje);
    var barva = zahteva.body.barva;
    var id = zahteva.params.idUrnik;

    if(!(ime) || !(ime.trim()))
        ime = "-";
    if(!(barva) || !(barva.trim()))
        barva = "/";

    if(dan < 0 || dan > 4) {
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Neveljaven dan. Mora biti v obsegu 0-4."
        });
    } else if(cas < 7 || cas > 20) {
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Neveljaven čas. Mora biti v obsegu 7-20."
        });
    } else if(trajanje < 1 || trajanje+cas > 21) {
        vrniJsonOdgovor(odgovor, 400, {
            "sporocilo": "Neveljavno trajanje. Mora biti vsaj 1, in ne sme presegati 21-cas."
        });
    } else Urnik.findByIdAndUpdate(id,
        {
            ime: ime,
            dan: dan,
            cas: cas,
            trajanje: trajanje,
            barva: barva
        },
        {new: true},
        function (napaka, uporabnik) {
            if (napaka) {
                vrniJsonOdgovor(odgovor, 400, {"sporocilo": "Prišlo je do napake"});
            } else {
                vrniJsonOdgovor(odgovor, 200, {"sporocilo": "Uspešno posodobljen vnos v urniku."});
            }
        });
};
