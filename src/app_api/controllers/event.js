var mongoose = require('mongoose');

var Event = mongoose.model('Event');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.eventSeznam = function (zahteva, odgovor) {
    Event
        .find({})
        .sort({'updatedAt':-1})
        .exec(function(error, rezultati) {
            if (error) {
                vrniJsonOdgovor(odgovor, 500, error);
            } else {
                if(rezultati.length>0){
                    vrniJsonOdgovor(odgovor, 200, rezultati);
                }
                else{
                    vrniJsonOdgovor(odgovor, 200, {
                        "sporocilo": "Ni dogodkov."
                    });
                }

            }
        });
};

module.exports.eventKreiraj = function(zahteva, odgovor) {
    Event.create({
        ime: zahteva.body.ime,
        organizator: zahteva.body.organizator,
        datum: zahteva.body.datum,
        opis: zahteva.body.opis
    }, function(napaka, Event) {
        if (napaka) {
          vrniJsonOdgovor(odgovor, 400, napaka);
        } else {
          vrniJsonOdgovor(odgovor, 201, Event);
        }
    });
};

module.exports.delete = function(req, res) {
    var id = req.params.id;
    if (id) {
        Event
            .findByIdAndRemove(id)
            .exec(
                function(error, product) {
                    if (error) {
                        vrniJsonOdgovor(res, 404, error);
                        return;
                    }
                    vrniJsonOdgovor(res, 204, null);
                }
            );
    } else {
        vrniJsonOdgovor(res, 400, {
            "message":
                "Event ni bil najden."
        });
    }
};
