var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var ObjectIdSchema = Schema.ObjectId;
var ObjectId = mongoose.Types.ObjectId;

var eventSchema = new mongoose.Schema({
    _id: {type:ObjectIdSchema, default: function () { return new ObjectId()} },
    ime: {type: String, required: true},
    datum:  {type: String, require: true},
    organizator: {type: String, required: true},
    opis: {type: String, required: true},
});

module.exports = eventSchema;

mongoose.model('Event', eventSchema, 'event');
