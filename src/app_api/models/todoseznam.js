var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var ObjectIdSchema = Schema.ObjectId;
var ObjectId = mongoose.Types.ObjectId;

var todoSchema = new mongoose.Schema({
    _id: {type:ObjectIdSchema, default: function () { return new ObjectId()} },
    userId : {type: Schema.Types.ObjectId, ref: 'Uporabniki'},
    email: {type: String, required: true},
    opis: {type: String, required: true}
});

module.exports = todoSchema;

mongoose.model('Todo', todoSchema, 'todo');