var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var ObjectIdSchema = Schema.ObjectId;
var ObjectId = mongoose.Types.ObjectId;

var urnikSchema = new mongoose.Schema({
    _id: {type:ObjectIdSchema, default: function () { return new ObjectId()} },
    userId : {type: Schema.Types.ObjectId, ref: 'Uporabniki'},
    ime: {type: String, required: true},
    dan: {type: Number, require: true},
    cas: {type: Number, require: true},
    trajanje: {type: Number, require: true},
    barva: {type: String}
});

module.exports = urnikSchema;

mongoose.model('Urnik', urnikSchema, 'urnik');