var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var ObjectIdSchema = Schema.ObjectId;
var ObjectId = mongoose.Types.ObjectId;

var obvestiloSchema = new mongoose.Schema({
    _id: {type:ObjectIdSchema, default: function () { return new ObjectId()} },
    obvestilo: {type: String},
});

module.exports = obvestiloSchema;

mongoose.model('Obvestilo', obvestiloSchema, 'obvestilo');  