var zahteva = require('../src/node_modules/supertest')

var streznik = require('../src/app')
var expect  = require('../src/node_modules/chai').expect;
var request = require('../src/node_modules/request');
var chai = require('../src/node_modules/chai');
var assert = require('assert');
var chaiHttp = require('../src/node_modules/chai-http');
chai.use(chaiHttp);

describe('Events page', function() {
    it('Events stran status', function(done) {
      chai.request(streznik)
          .get('/events')
          .end(function(err, res){
            expect(res).to.have.status(200);
            done();
          });
    });

    it('Events manager stran status', function(done) {
      chai.request(streznik)
          .get('/events/manager')
          .end(function(err, res){
            expect(res).to.have.status(200);
            done();
          });
    });

      it('it should POST new event with correctly filled data', (done) => {
          let event = {
              ime: "The Lord of the Rings",
              datum: "1.1.2019",
              organizator: "1954",
              opis: "sqweqwe"
          }
        chai.request(streznik)
            .post('/events')
            .send(event)
            .end((err, res) => {
              expect(res).to.have.status(200);
              assert.equal(res.req.path, "/events?uspeh=true");
              done();
            });
      });

      it('it should NOT POST new event without all input fields filled', (done) => {
            let event = {
                ime: "The Lord of the Rings",
                datum: "1.1.2019",
                organizator: "1954",
                opis: ""
            }
          chai.request(streznik)
              .post('/events')
              .send(event)
              .end((err, res) => {
                expect(res).to.have.status(200);
                assert.equal(res.req.path, "/events?uspeh=false");
                done();
              });
      });
});

describe('Bus page', function() {
    it('Bus stran status', function(done) {
      chai.request(streznik)
          .get('/bus')
          .end(function(err, res){
            expect(res).to.have.status(200);
            done();
          });
    });


      it('it should return buses at station that EXISTS', (done) => {
          let bus = {
              postaja: "Rudnik"
          }
        chai.request(streznik)
            .post('/isciBus')
            .send(bus)
            .end((err, res) => {
              expect(res).to.have.status(200);
              done();
            });
      });

      it('it should NOT return buses if a station exists', (done) => {
            let bus = {
                postaja: "UpamDaNeObstajam"
            }
          chai.request(streznik)
              .post('/isciBus')
              .send(bus)
              .end((err, res) => {
                expect(res).to.have.status(200);
                done();
              });
      });
});


describe('Prijava page', () => {
    it('Prijava stran status', function(done) {
          chai.request(streznik)
              .get('/prijava')
              .end(function(err, res){
                expect(res).to.have.status(200);
                done();
              });
        });

      it('it should return fine with the right supplied user data', (done) => {
          let prijava = {
              email: "manager@gmail.com",
              geslo: "abc2"
          }
        chai.request(streznik)
            .post('/prijava')
            .send(prijava)
            .end((err, res) => {
              expect(res).to.have.status(200);
              assert.equal(res.req.path, "/prijava");
              done();
            });
      });

      it('it should redirect user 3 to events page', (done) => {
            let prijava = {
                email: "manager@gmail.com",
                geslo: "abc"
            }
          chai.request(streznik)
              .post('/prijava')
              .send(prijava)
              .end((err, res) => {
                expect(res).to.have.status(200);
                assert.equal(res.req.path, "/events");
                done();
              });
        });
});

function makeid(length) {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}

describe('Registracija page', () => {
    it('Registracija stran status', function(done) {
          chai.request(streznik)
              .get('/registracija')
              .end(function(err, res){
                expect(res).to.have.status(200);
                done();
              });
        });

      /*it('it should return fine with the right supplied data to input fields', (done) => {
          let registracija = {
              ime: "lojzi",
              priimek: "strle",
              email: makeid(10) + "@gmail.com",
              geslo: "asdad12aa",
              geslo_potrdi: "asdad12aa"
          }
        chai.request(streznik)
            .post('/registracija')
            .send(registracija)
            .end((err, res) => {
              expect(res).to.have.status(200);
              assert.equal(res.req.path, "/prijava");
              done();
            });
      });

      it('it should return fine with the right supplied data', (done) => {
            let prijava = {
                email: "manager@gmail.com",
                geslo: "abc"
            }
          chai.request(streznik)
              .post('/prijava')
              .send(prijava)
              .end((err, res) => {
                expect(res).to.have.status(200);
                assert.equal(res.req.path, "/events");
                done();
              });
        });*/
});

describe('Uporabnik page', () => {
    it('Uporabnik stran status', function(done) {
          chai.request(streznik)
              .get('/uporabnik')
              .end(function(err, res){
                expect(res).to.have.status(200);
                done();
              });
        });

});

describe('Home page', () => {
    it('Home stran status', function(done) {
          chai.request(streznik)
              .get('/')
              .end(function(err, res){
                expect(res).to.have.status(200);
                done();
              });
        });
});

describe('Food page', () => {
    it('Food stran status', function(done) {
          chai.request(streznik)
              .get('/food')
              .end(function(err, res){
                expect(res).to.have.status(200);
                done();
              });
        });
});

